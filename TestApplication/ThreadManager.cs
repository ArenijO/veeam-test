﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Xml;

namespace TestApplication
{
    class ThreadManager
    {
        public readonly ThreadBlockQueue Queue;

        public ThreadManager(ThreadBlockQueue bq)
        {
            Queue = bq;
        }

        public void Start(OutputHash output)
        {
            while (Queue.Dequeue(out var dataToHash))
            {
                CalculateHash(dataToHash, output);
            }
        }

        private void CalculateHash(Block hashData, OutputHash output)
        {
            var sha = new SHA256Managed();
            var result = sha.ComputeHash(hashData.block);
            output.Add(new HashedBlock(hashData.id, result));
        }
    }
}
