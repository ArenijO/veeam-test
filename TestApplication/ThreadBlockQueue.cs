﻿using System.Collections.Generic;
using System.Threading;

namespace TestApplication
{
    class ThreadBlockQueue
    {
        public bool ConnectionClosed = false;
        public Queue<Block> BlockQueue;
        readonly int _size;

        public ThreadBlockQueue(int queueSize = 64)
        {
            _size = queueSize;
            BlockQueue = new Queue<Block>();
        }
        public void AddBlock(Block block)
        {
            lock (BlockQueue)
            {
                while (true)
                {
                    if (BlockQueue.Count < _size)
                    {
                        BlockQueue.Enqueue(block);
                        Monitor.Pulse(BlockQueue);
                        break;
                    }

                    Monitor.Wait(BlockQueue);
                }
                if(ConnectionClosed)
                    Monitor.PulseAll(BlockQueue);
            }
        }

        public bool Dequeue(out Block itemBlock)
        {
            lock (BlockQueue)
            {
                while (true)
                {
                    if (BlockQueue.Count > 0)
                    {
                        itemBlock = BlockQueue.Dequeue();
                        Monitor.Pulse(BlockQueue);
                        return true;
                    }
                    if (ConnectionClosed)
                    {
                        itemBlock = null;
                        return false;
                    }
                    Monitor.Wait(BlockQueue);
                }
            }
        }
    }
}
