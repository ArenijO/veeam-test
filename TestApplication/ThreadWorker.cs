﻿using System.Security.Cryptography;

namespace TestApplication
{
    class ThreadWorker
    {
        public readonly BlockQueue Queue;

        public ThreadWorker(BlockQueue bq)
        {
            Queue = bq;
        }

        public void Start(OutputHashCollection output)
        {
            while (Queue.Dequeue(out var dataToHash))
            {
                try
                {
                    CalculateHash(dataToHash, output);
                }
                catch (System.Exception ex)
                {
                    var log = ErrorLogger.GetInstance();
                    log.AddException(ex.Message, ex.StackTrace);

                }


            }
        }

        private void CalculateHash(Block hashData, OutputHashCollection output)
        {
            var sha = new SHA256Managed();
            var result = sha.ComputeHash(hashData.block);
            output.Add(new HashedBlock(hashData.id, result));
        }
    }
}
