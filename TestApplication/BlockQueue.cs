﻿using System.Collections.Generic;
using System.Threading;

namespace TestApplication
{
    class BlockQueue
    {
        public bool ConnectionClosed = false;
        private readonly Queue<Block> _blockQueue;
        readonly int _size;

        public BlockQueue(int queueSize = 64)
        {
            _size = queueSize;
            _blockQueue = new Queue<Block>();
        }

        public void AddBlock(Block block)
        {
            lock (_blockQueue)
            {
                while (true)
                {
                    if (_blockQueue.Count < _size)
                    {
                        _blockQueue.Enqueue(block);
                        Monitor.Pulse(_blockQueue);
                        break;
                    }

                    Monitor.Wait(_blockQueue);
                }
                if(ConnectionClosed)
                    Monitor.PulseAll(_blockQueue);
            }
        }

        public bool Dequeue(out Block itemBlock)
        {
            lock (_blockQueue)
            {
                while (true)
                {
                    if (_blockQueue.Count > 0)
                    {
                        itemBlock = _blockQueue.Dequeue();
                        Monitor.Pulse(_blockQueue);
                        return true;
                    }
                    if (ConnectionClosed)
                    {
                        itemBlock = null;
                        return false;
                    }
                    Monitor.Wait(_blockQueue);
                }
            }
        }
    }
}
