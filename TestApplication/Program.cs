﻿using System;
using System.IO;
using System.Threading;

namespace TestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
             if (args.Length < 2)
             {
                 Console.WriteLine("Wrong syntax. Use: TestApplication.exe <filePath> <blockSize>");
                 Environment.Exit(1);
             }

             string fileName = args[0];
           
             if (!File.Exists(fileName))
             {
                 Console.Error.WriteLine("File not found. Please, try again");
                 Console.Error.WriteLine("Use: TestApplication.exe <filePath> <blockSize>");
                 Environment.Exit(2);
             }

             if (!int.TryParse(args[1], out var blockSize))
             {
                 Console.Error.WriteLine("Invalid block size.");
                 Environment.Exit(2);
             }

            var processorCounts = Environment.ProcessorCount;
            var logs = ErrorLogger.GetInstance();
            BlockQueue tbq = new BlockQueue(processorCounts * 10);
            OutputHashCollection output = new OutputHashCollection();

            var threads = new Thread[processorCounts];

            for (int j = 0; j < threads.Length; j++)
            {
                Thread thread = new Thread((() => new ThreadWorker(tbq).Start(output)));
                thread.Start();
                threads[j] = thread;
            }

            Thread readThread = new Thread((() => new FileReader().ReadBlocks(fileName, tbq, blockSize)));
            readThread.Start();

            foreach (Thread thread in threads)
            {
                thread.Join();
            }

            if (logs.HasErrors())
            {
                Console.WriteLine("The following errors was found:");
                logs.PrintErrors();
            }
            else
            {
                output.PrintResult();
            }

            Console.Read();
        }
    }
}

