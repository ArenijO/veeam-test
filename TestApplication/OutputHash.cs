﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TestApplication
{
    class OutputHash
    {
        public List<HashedBlock> Output;

        public OutputHash()
        {
            Output = new List<HashedBlock>();
        }

        public void Add(HashedBlock hashedBlock)
        {
            lock (Output)
            {
                Output.Add(hashedBlock);
            }
        }

        public void PrintResult()
        {
            lock (Output)
            {
                foreach (var data in Output.OrderBy(b => b.id))
                {
                    Console.Write("Block: {0}, hash: ", data.id);
                    foreach (var blockByte in data.block)
                    {
                        Console.Write("{0:X}", blockByte);
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
