﻿using System;
using System.Collections.Generic;

namespace TestApplication
{

    class ErrorLogger
    {
        private static readonly object _lock = new object();
        private readonly List<string> _errorDictionary = new List<string>();
        private static ErrorLogger _log;
        private ErrorLogger() {}

        public void AddException(string message, string stackTrace)
        {
            _log._errorDictionary.Add(message + "StackTrace:" + stackTrace);
        }

        public static ErrorLogger GetInstance()
        {
            if (_log == null)
            {
                lock (_lock)
                {
                    if (_log == null)
                    {
                        _log = new ErrorLogger();
                    }
                }
            }

            return _log;
        }

        public bool HasErrors()
        {
           return _errorDictionary.Count > 0;
        }

        public void PrintErrors()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            foreach (var error in _errorDictionary)
            {
                Console.WriteLine(error + "::::");
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
