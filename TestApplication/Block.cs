﻿namespace TestApplication
{
    class Block
    {
        public int id;
        public byte[] block;

        public Block(int id, byte[] data)
        {
            this.id = id;
            this.block = data;;
        }
    }
}
