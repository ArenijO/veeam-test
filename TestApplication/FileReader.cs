﻿using System;
using System.IO;

namespace TestApplication
{
    class FileReader
    { 
        public void ReadBlocks(string filePath, BlockQueue tbq, int blockSize = 256)
        {
            try
            {
                using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    bool isLastBlock;
                    int id = 0;
                    // Read block size.
                    do
                    {
                        var lengthBytes = new byte[blockSize];
                        var bytesCount = stream.Read(lengthBytes, 0, lengthBytes.Length);
                        tbq.AddBlock(new Block(id, lengthBytes));
                        id++;

                        isLastBlock = bytesCount == 0;
                    } while (!isLastBlock);

                    tbq.ConnectionClosed = true;
                }
            }
            catch (Exception e)
            {
                var logs = ErrorLogger.GetInstance();
                logs.AddException(e.Message, e.StackTrace);
            }
        }
    }
}
